//
//  Docs.swift
//  Runner
//
//  Created by Tharani on 11/04/19.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import Foundation
import RealmSwift

//Realm Model
class Docs: Object {
    @objc dynamic var rowId : Int = 0
    @objc dynamic var ORDERNUM_s : String = ""
    @objc dynamic var OUNUM_s : String = ""
    @objc dynamic var id: String = ""
    @objc dynamic var CONNECTIONTYPE_s = ""
    @objc dynamic var SERIALNUM_s = ""
    @objc dynamic var ASSETROWID_s: String = ""
    @objc dynamic var DATECOL_dt: String = ""
    @objc dynamic var NAMEREF_s = ""
    @objc dynamic var REGDATE_dt: String = ""
    @objc dynamic var ROOTASSET_s: String = ""
    @objc dynamic var DESCTEXT_s = ""
    @objc dynamic var BCN_s:String = ""
    @objc dynamic var ROW_ID_s: String = ""
    @objc dynamic var EXTNAME_s: String = ""
    @objc dynamic var ASSETNUM_s: String = ""
    @objc dynamic var PRIMARYID_s: String = ""
    @objc dynamic var ROOTSERVICEID_s: String = ""
    @objc dynamic var DATECOL1_s:String = ""
    @objc dynamic var ADDRESS_s: String = ""
    @objc dynamic var OCN_s: String = ""
    @objc dynamic var REGDATE1_s: String = ""
    @objc dynamic var LEGALNAME_s:String = ""
    @objc dynamic var _version_: String = ""
    
    override static func primaryKey() -> String? {
        return "rowId"
    }
    
    convenience init(rowId:Int, oRDERNUM_s : String, oUNUM_s: String, id : String, cONNECTIONTYPE_s: String, sERIALNUM_s : String, aSSETROWID_s: String, dATECOL_dt: String, nAMEREF_s: String, rEGDATE_dt: String, rOOTASSET_s: String, dESCTEXT_s: String, bCN_s : String, rOW_ID_s: String, eXTNAME_s: String, aSSETNUM_s :String, pRIMARYID_s : String, rOOTSERVICEID_s : String, dATECOL1_s : String, aDDRESS_s : String, OCN_s: String,rEGDATE1_s: String,lEGALNAME_s: String, version:String) {
        self.init()
        self.rowId = rowId
        self.ORDERNUM_s = oRDERNUM_s
        self.OUNUM_s = oUNUM_s
        self.id = id
        self.CONNECTIONTYPE_s = cONNECTIONTYPE_s
        self.SERIALNUM_s = sERIALNUM_s
        self.ASSETROWID_s = aSSETROWID_s
        self.DATECOL_dt = dATECOL_dt
        self.NAMEREF_s = nAMEREF_s
        self.REGDATE_dt = rEGDATE_dt
        self.ROOTASSET_s = rOOTASSET_s
        self.DESCTEXT_s = dESCTEXT_s
        self.BCN_s = bCN_s
        self.ROW_ID_s = rOW_ID_s
        self.EXTNAME_s = eXTNAME_s
        self.ASSETNUM_s = aSSETNUM_s
        self.PRIMARYID_s = pRIMARYID_s
        self.ROOTSERVICEID_s = rOOTSERVICEID_s
        self.DATECOL1_s = dATECOL1_s
        self.ADDRESS_s = aDDRESS_s
        self.OCN_s = OCN_s
        self.REGDATE1_s = rEGDATE1_s
        self.LEGALNAME_s = lEGALNAME_s
        self._version_ = version
    }
    
    //Incrementa ID
    
    func incrementID(realm:Realm) -> Int {
        return (realm.objects(Docs.self).max(ofProperty: "rowId") as Int? ?? 0) + 1
    }

}
