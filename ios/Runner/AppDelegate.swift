import UIKit
import Flutter
import RealmSwift

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    var realm = try! Realm()
    var docsArray = NSMutableArray();
    var startDate = Date()
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
        ) -> Bool {
        print("DB PATH")
        print(realm.configuration.fileURL?.path)
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        let batteryChannel = FlutterMethodChannel(name: "FlutterWithRealm",binaryMessenger: controller)
        batteryChannel.setMethodCallHandler({ [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if (call.method == "FetchDocsData") {
                self!.fetchDocsInRealm(result: result)
            }
            else if (call.method == "ResponseDataToStore") {
                let arguments = call.arguments as? NSDictionary // INFO: get arguments
                self!.saveDocsToRealm(result: result, data: arguments?.value(forKey: "param") as? NSArray)
            }
        })
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func stringFromTimeInterval(interval: Double) -> NSString {
        let hours = (Int(interval) / 3600)
        let minutes = Int(interval / 60) - Int(hours * 60)
        let seconds = Int(interval) - (Int(interval / 60) * 60)
        return NSString(format: "%0.2d",seconds)
    }

    //To Store docs info to DB
    func saveDocsToRealm(result: FlutterResult, data: NSArray?) {
        try? realm.write {
            realm.deleteAll()
        }
        storeDocsDataToRealm(result: result, data: data)
        result(docsArray)
    }
    
    func storeDocsDataToRealm(result: FlutterResult, data: NSArray?) {
        if let docs = data {
            print("DOCS COUNT TO SAVE \(docs.count)")
            startDate = Date()
            for (_,aDoc) in docs.enumerated()  {
                let tempDict = aDoc as? NSDictionary
                if let tempDict = tempDict {
                    let docsModel = convertDictionaryToDocsModel(aDocDict: tempDict)
                    saveObjects(objs: docsModel)
                }
            }
            let executionTime = Date().timeIntervalSince(startDate)
            print("Saved Timig in Sec::: \(stringFromTimeInterval(interval: executionTime))")
        }
        getSavedDocsFromRealm()
    }
    
    //Fetch From Realm
    func fetchDocsInRealm(result: FlutterResult) {
        getSavedDocsFromRealm()
       
        result(docsArray) // INFO: success response should return through this method
    }

    //To get all data From Realm
    func getSavedDocsFromRealm() {
        docsArray.removeAllObjects()
        startDate = Date()
        for var aDoc in realm.objects(Docs.self) {
            let dict = convertDocsModelToDictionary(docModel: aDoc)
            docsArray.add(dict)
        }
        let executionTime = Date().timeIntervalSince(startDate)
        startDate = Date()
        print("Fetched Timig in Sec::: \(self.stringFromTimeInterval(interval: executionTime))")
    }
    
    //To convert Dictionary to Docs Model
    func convertDictionaryToDocsModel(aDocDict : NSDictionary) -> Docs {
        let docModel = Docs()
        docModel.ORDERNUM_s = aDocDict["ORDERNUM_s"] as? String ?? ""
        docModel.OUNUM_s = aDocDict["OUNUM_s"] as? String ?? ""
        docModel.id = aDocDict["id"] as? String ?? ""
        docModel.CONNECTIONTYPE_s = aDocDict["CONNECTIONTYPE_s"]  as? String ?? ""
        docModel.SERIALNUM_s = aDocDict["SERIALNUM_s"] as? String ?? ""
        docModel.ASSETROWID_s = aDocDict["ASSETROWID_s"] as? String ?? ""
        docModel.DATECOL_dt = aDocDict["DATECOL_dt"] as? String ?? ""
        docModel.NAMEREF_s = aDocDict["NAMEREF_s"] as? String ?? ""
        docModel.REGDATE_dt = aDocDict["REGDATE_dt"] as? String ?? ""
        docModel.ROOTASSET_s = aDocDict["ROOTASSET_s"] as? String ?? ""
        docModel.DESCTEXT_s = aDocDict["DESCTEXT_s"] as? String ?? ""
        docModel.BCN_s = aDocDict["BCN_s"] as? String ?? ""
        docModel.ROW_ID_s = aDocDict["ROW_ID_s"] as? String ?? ""
        docModel.EXTNAME_s = aDocDict["EXTNAME_s"] as? String ?? ""
        docModel.ASSETNUM_s = aDocDict["ASSETNUM_s"] as? String ?? ""
        docModel.PRIMARYID_s = aDocDict["PRIMARYID_s"] as? String ?? ""
        docModel.ROOTSERVICEID_s = aDocDict["ROOTSERVICEID_s"] as? String ?? ""
        docModel.DATECOL1_s = aDocDict["DATECOL1_s"] as? String ?? ""
        docModel.ADDRESS_s = aDocDict["ADDRESS_s"] as? String ?? ""
        docModel.OCN_s = aDocDict["OCN_s"] as? String ?? ""
        docModel.REGDATE1_s = aDocDict["REGDATE1_s"] as? String ?? ""
        docModel.LEGALNAME_s = aDocDict["LEGALNAME_s"] as? String ?? ""
        docModel._version_ = aDocDict["_version_"] as? String ?? ""
        docModel.rowId = docModel.incrementID(realm: realm)
        return docModel
    }
    
    func convertDocsModelToDictionary(docModel: Docs) -> [String : Any] {
        var aDocDict = [String : Any]()
        aDocDict["ORDERNUM_s"] = docModel.ORDERNUM_s
        aDocDict["OUNUM_s"] = docModel.OUNUM_s
        aDocDict["id"] = docModel.id
        aDocDict["CONNECTIONTYPE_s"] = docModel.CONNECTIONTYPE_s
        aDocDict["SERIALNUM_s"] = docModel.SERIALNUM_s
        aDocDict["ASSETROWID_s"] = docModel.ASSETROWID_s
        aDocDict["DATECOL_dt"] = docModel.DATECOL_dt
        aDocDict["NAMEREF_s"] = docModel.NAMEREF_s
        aDocDict["REGDATE_dt"] = docModel.REGDATE_dt
        aDocDict["ROOTASSET_s"] = docModel.ROOTASSET_s
        aDocDict["DESCTEXT_s"] = docModel.DESCTEXT_s
        aDocDict["BCN_s"] = docModel.BCN_s
        aDocDict["ROW_ID_s"] = docModel.ROW_ID_s
        aDocDict["EXTNAME_s"] = docModel.EXTNAME_s
        aDocDict["ASSETNUM_s"] = docModel.ASSETNUM_s
        aDocDict["PRIMARYID_s"] = docModel.PRIMARYID_s
        aDocDict["ROOTSERVICEID_s"] = docModel.ROOTSERVICEID_s
        aDocDict["DATECOL1_s"] = docModel.DATECOL1_s
        aDocDict["ADDRESS_s"] = docModel.ADDRESS_s
        aDocDict["OCN_s"] = docModel.OCN_s
        aDocDict["REGDATE1_s"] = docModel.REGDATE1_s
        aDocDict["LEGALNAME_s"] = docModel.LEGALNAME_s
        aDocDict["_version_"] = docModel._version_
        return aDocDict
    }
    
    // Save array of objects to database
    func saveObjects(objs: Object) {
        try? realm.write ({
            // If update = false, adds the object
            realm.add(objs, update: false)
        })
    }
}
//END
