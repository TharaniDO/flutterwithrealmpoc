// To parse this JSON data, do
//
//     final responseModel = responseModelFromJson(jsonString);

import 'dart:convert';

List<ResponseModel> responseModelFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<ResponseModel>.from(jsonData.map((x) => ResponseModel.fromJson(x)));
}

String responseModelToJson(List<ResponseModel> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class ResponseModel {
  String id;
  String employeeName;
  String employeeSalary;
  String employeeAge;
  String profileImage;

  ResponseModel({
    this.id,
    this.employeeName,
    this.employeeSalary,
    this.employeeAge,
    this.profileImage,
  });

  factory ResponseModel.fromJson(Map<String, dynamic> json) => new ResponseModel(
    id: json["id"] == null ? null : json["id"],
    employeeName: json["employee_name"] == null ? null : json["employee_name"],
    employeeSalary: json["employee_salary"] == null ? null : json["employee_salary"],
    employeeAge: json["employee_age"] == null ? null : json["employee_age"],
    profileImage: json["profile_image"] == null ? null : json["profile_image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "employee_name": employeeName == null ? null : employeeName,
    "employee_salary": employeeSalary == null ? null : employeeSalary,
    "employee_age": employeeAge == null ? null : employeeAge,
    "profile_image": profileImage == null ? null : profileImage,
  };
}
