import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:collection';
import  'package:flutterwithrealm/apiModel.dart';
import 'package:flutterwithrealm/apiResponseModel.dart';
import 'dart:convert';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  static const channelName = const MethodChannel('FlutterWithRealm');
  List<dynamic> docList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _getCompanyInfo();
    _fetchDataInitialFromRealm();
  }


  //Fetch Initally Data through channel
  Future _fetchDataInitialFromRealm() async {
    print('Inital call to Native');
      docList = [];
      docList = await channelName.invokeMethod('FetchDocsData');
      print('Initial Fetched Docs');
      print(docList.length);
      print('Intial End Fetched Docs!!!');
  }

  //API Call to get response
  Future _apiCall() async  {
    ApiModelClass apiModel = ApiModelClass();
    String url = 'https://dcp.colt.net/portal/mflash';
    await apiModel.requestInGet(url, null).then((response) {
      Map<String,dynamic> data = json.decode(response.body);
      Map<String,dynamic> responsedata = data['response'];
      List<dynamic> map = responsedata['docs'];
      _onLoading();
      _storeDataInNativeThroughChannel(map);
    }).catchError((onError){
       showAlertDialog(context, '$onError');
    });
  }

  //Call Native Method through channel
  Future _storeDataInNativeThroughChannel(List map) async {
    print('Calling Native');
    if (map != null) {
      docList = [];
      Navigator.pop(context);
      docList = await channelName.invokeMethod('ResponseDataToStore', <String, dynamic>{
        'param' : map
      });
      print('Start Fetched Docs');
      print(docList.length);
      print('End Fetched Docs!!!');

    } else {
      showAlertDialog(context, 'Please enter the input.');
    }
  }

  //Loader
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      child: new Dialog(
        child: new Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            new CircularProgressIndicator(),
            new Text("Loading"),
          ],
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context, String message) {
    Widget okButton =
    Center(
      child: Row(
        children: <Widget>[
          FlatButton(
            color: Colors.white,
            child: Text("Ok", style: TextStyle(color: Color.fromRGBO(29, 233, 182, 1),fontFamily: 'Nunito Sans', fontSize: 15, fontWeight: FontWeight.w600),),
            onPressed: () async {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );

    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text(message),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Flutter With Realm"),),
        body:
        Column(
          children: <Widget>[
             RaisedButton(
               child: Text("Click here to Save data to Realm."),
               onPressed: (){
                _apiCall();
             },)
            ]
        )
    );
  }
}