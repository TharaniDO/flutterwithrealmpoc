import 'package:http/http.dart' as http;

class ApiModelClass {
  String contentType = 'application/json';
  String authrizationToken = '';

  //Get Method
  Future<http.Response> requestInGet(String url, String token) async {
    final response = await http.get(url ,headers: { 'X-COLLECTION' : 'service',
                                                    'X-TOKEN' : 'PriWAmTNpOs9lxkxjVCJ0kjXRzrglXJ0y1+s692u4q1oeOYr/YBShrhw7uTwFlFbNlHkT464Uh1Ry4+hr2kRJA==',
                                                    'X-QUERY' : 'indent=on&q=*:*&wt=json&sort=DATECOL_dt desc&start=0&rows=2000',
                                                    'Content-Type' : 'application/json'});
    if (response != null) {
      if (response.statusCode == 200) {
        print('got response');
        return response;
      } else {
        throw Exception('Failed to load data');
      }
    }
  }

//Post method
  Future<http.Response> requestInPost(String url, String token) async {

    final response = await http.post(url, headers: {
      'Accept' : contentType, 'Authorization' : authrizationToken}).timeout(Duration(seconds: 30));
    if (response != null) {
      if (response.statusCode == 200) {
        print(response.body);
        return response;
      } else {
        throw Exception('Failed to load data');
      }
    }
  }

  //Post method
  Future<http.Response> requestInPut(String url, String token) async {

    final response = await http.put(url, headers: {
      'Accept' : contentType, 'Authorization' : authrizationToken}).timeout(Duration(seconds: 30));
    if (response != null) {
      if (response.statusCode == 200) {
        print(response.body);
        return response;
      } else {
        throw Exception('Failed to load data');
      }
    }
  }
}