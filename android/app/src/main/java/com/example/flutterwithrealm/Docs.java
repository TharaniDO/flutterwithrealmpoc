package com.example.flutterwithrealm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Docs extends RealmObject  {
    @PrimaryKey  private int rowId;
    private String ORDERNUM_s;
    private String OUNUM_s;
    private String id;
    private String CONNECTIONTYPE_s;
    private String SERIALNUM_s;
    private String ASSETROWID_s;
    private String DATECOL_dt;
    private String NAMEREF_s;
    private String REGDATE_dt;
    private String ROOTASSET_s;
    private String DESCTEXT_s;
    private String BCN_s;
    private String ROW_ID_s;
    private String EXTNAME_s;
    private String ASSETNUM_s;
    private String PRIMARYID_s;
    private String ROOTSERVICEID_s;
    private String DATECOL1_s;
    private String ADDRESS_s;
    private String OCN_s;
    private String REGDATE1_s;
    private String LEGALNAME_s;
    private String _version;
    public Docs() {

    }

    public String getORDERNUM_s() {
        return ORDERNUM_s;
    }

    public void setORDERNUM_s(String ORDERNUM_s) {
        this.ORDERNUM_s = ORDERNUM_s;
    }

    public String getOUNUM_s() {
        return OUNUM_s;
    }

    public void setOUNUM_s(String OUNUM_s) {
        this.OUNUM_s = OUNUM_s;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCONNECTIONTYPE_s() {
        return CONNECTIONTYPE_s;
    }

    public void setCONNECTIONTYPE_s(String CONNECTIONTYPE_s) {
        this.CONNECTIONTYPE_s = CONNECTIONTYPE_s;
    }

    public String getSERIALNUM_s() {
        return SERIALNUM_s;
    }

    public void setSERIALNUM_s(String SERIALNUM_s) {
        this.SERIALNUM_s = SERIALNUM_s;
    }

    public String getASSETROWID_s() {
        return ASSETROWID_s;
    }

    public void setASSETROWID_s(String ASSETROWID_s) {
        this.ASSETROWID_s = ASSETROWID_s;
    }

    public String getDATECOL_dt() {
        return DATECOL_dt;
    }

    public void setDATECOL_dt(String DATECOL_dt) {
        this.DATECOL_dt = DATECOL_dt;
    }

    public String getNAMEREF_s() {
        return NAMEREF_s;
    }

    public void setNAMEREF_s(String NAMEREF_s) {
        this.NAMEREF_s = NAMEREF_s;
    }

    public String getREGDATE_dt() {
        return REGDATE_dt;
    }

    public void setREGDATE_dt(String REGDATE_dt) {
        this.REGDATE_dt = REGDATE_dt;
    }

    public String getROOTASSET_s() {
        return ROOTASSET_s;
    }

    public void setROOTASSET_s(String ROOTASSET_s) {
        this.ROOTASSET_s = ROOTASSET_s;
    }

    public String getDESCTEXT_s() {
        return DESCTEXT_s;
    }

    public void setDESCTEXT_s(String DESCTEXT_s) {
        this.DESCTEXT_s = DESCTEXT_s;
    }

    public String getBCN_s() {
        return BCN_s;
    }

    public void setBCN_s(String BCN_s) {
        this.BCN_s = BCN_s;
    }

    public String getROW_ID_s() {
        return ROW_ID_s;
    }

    public void setROW_ID_s(String ROW_ID_s) {
        this.ROW_ID_s = ROW_ID_s;
    }

    public String getEXTNAME_s() {
        return EXTNAME_s;
    }

    public void setEXTNAME_s(String EXTNAME_s) {
        this.EXTNAME_s = EXTNAME_s;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public String getASSETNUM_s() {
        return ASSETNUM_s;
    }

    public void setASSETNUM_s(String ASSETNUM_s) {
        this.ASSETNUM_s = ASSETNUM_s;
    }

    public String getPRIMARYID_s() {
        return PRIMARYID_s;
    }

    public void setPRIMARYID_s(String PRIMARYID_s) {
        this.PRIMARYID_s = PRIMARYID_s;
    }

    public String getROOTSERVICEID_s() {
        return ROOTSERVICEID_s;
    }

    public void setROOTSERVICEID_s(String ROOTSERVICEID_s) {
        this.ROOTSERVICEID_s = ROOTSERVICEID_s;
    }

    public String getDATECOL1_s() {
        return DATECOL1_s;
    }

    public void setDATECOL1_s(String DATECOL1_s) {
        this.DATECOL1_s = DATECOL1_s;
    }

    public String getADDRESS_s() {
        return ADDRESS_s;
    }

    public void setADDRESS_s(String ADDRESS_s) {
        this.ADDRESS_s = ADDRESS_s;
    }

    public String getOCN_s() {
        return OCN_s;
    }

    public void setOCN_s(String OCN_s) {
        this.OCN_s = OCN_s;
    }

    public String getREGDATE1_s() {
        return REGDATE1_s;
    }

    public void setREGDATE1_s(String REGDATE1_s) {
        this.REGDATE1_s = REGDATE1_s;
    }

    public String getLEGALNAME_s() {
        return LEGALNAME_s;
    }

    public void setLEGALNAME_s(String LEGALNAME_s) {
        this.LEGALNAME_s = LEGALNAME_s;
    }

    public String get_version() {
        return _version;
    }

    public void set_version(String _version) {
        this._version = _version;
    }
}
