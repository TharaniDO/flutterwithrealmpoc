package com.example.flutterwithrealm;
import io.realm.Realm;
import io.flutter.app.FlutterActivity;
import io.realm.RealmConfiguration;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import android.os.Bundle;
import java.lang.*;
import java.util.*;

import io.flutter.plugins.GeneratedPluginRegistrant;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MainActivity extends FlutterActivity {

  //Realm realm;
    ArrayList<HashMap<String, String>> docsArray = new ArrayList<HashMap<String, String>>();
    ObjectMapper oMapper = new ObjectMapper();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
      Realm.init(this);
      Realm realm = Realm.getDefaultInstance();
      GeneratedPluginRegistrant.registerWith(this);
      new MethodChannel(getFlutterView(), "FlutterWithRealm").setMethodCallHandler(
            new MethodCallHandler() {
              @Override
              public void onMethodCall(MethodCall call, Result result) {

                  if (call.method.equals("FetchDocsData")) {
                      fetchFunction(result,realm);
                  }
                  else if (call.method.equals("ResponseDataToStore")) {
                      Map map = oMapper.convertValue(call.arguments, Map.class);
                      List<Map> paramList = (List<Map>) map.get("param");
                      saveDocsInRealm(result, paramList,realm);
                }
              }
            });
  }

  void fetchFunction(Result result, Realm realm) {
      String path = realm.getPath();
      if (true) {
          long start = System.currentTimeMillis();
          fetchFromRealm(realm);
          long end = System.currentTimeMillis();
          System.out.println(" Fetching TIME Docs TO DB :: " + (end-start) + " millis");
          result.success(docsArray);
      } else {
          result.error("fetchDocsInfo", "Docs Info not available.", null);
      }
  }

  void saveDocsInRealm(Result result, List<Map> data, Realm realm) {
      realm.executeTransaction(new Realm.Transaction() {
          @Override
          public void execute(Realm realm) {
              realm.deleteAll();}
      });
      if (data != null) {
          long start = System.currentTimeMillis();
          saveDataToRealm( data, realm);
          long end = System.currentTimeMillis();
          System.out.println(" SAVING TIME Docs TO DB :: " + (end-start) + " millis");
          result.success(docsArray);
      }
  }

    void saveDataToRealm( List<Map> data, Realm realm) {
        if (data != null) {
            System.out.print("SAVING EMPLOYEE COUNT :: " + data.size());
            for(int i = 0;i<data.size();i++) {
                Map map = (Map) data.get(i);
                if (map != null) {
                    realm.beginTransaction();
                    // increment index
                    Number num = realm.where(Docs.class).max("rowId");
                    int nextID;
                    if(num == null) {
                        nextID = 1;
                    } else {
                        nextID = num.intValue() + 1;
                    }
                    Docs docs = realm.createObject(Docs.class, nextID);
                    docs.setORDERNUM_s(map.get("ORDERNUM_s") != null ? map.get("ORDERNUM_s").toString() : "");
                    docs.setOUNUM_s(map.get("OUNUM_s") != null ? map.get("OUNUM_s").toString() : "");
                    docs.setId(map.get("id") !=null ? map.get("id").toString() : "");
                    docs.setCONNECTIONTYPE_s(map.get("CONNECTIONTYPE_s") != null ? map.get("CONNECTIONTYPE_s").toString() : "");
                    docs.setSERIALNUM_s(map.get("SERIALNUM_s") != null ? map.get("SERIALNUM_s").toString() : "");
                    docs.setASSETROWID_s(map.get("ASSETROWID_s") != null ? map.get("ASSETROWID_s").toString() : "");
                    docs.setDATECOL_dt(map.get("DATECOL_dt") != null ? map.get("DATECOL_dt").toString() : "");
                    docs.setNAMEREF_s(map.get("NAMEREF_s") != null ? map.get("NAMEREF_s").toString() : "");
                    docs.setREGDATE_dt(map.get("REGDATE_dt") != null ? map.get("REGDATE_dt").toString() : "");
                    docs.setROOTASSET_s(map.get("ROOTASSET_s") != null ? map.get("ROOTASSET_s").toString(): "");
                    docs.setDESCTEXT_s(map.get("DESCTEXT_s") != null ? map.get("DESCTEXT_s").toString(): "");
                    docs.setBCN_s(map.get("BCN_s") != null ? map.get("BCN_s").toString() : "");
                    docs.setROW_ID_s(map.get("ROW_ID_s") != null ? map.get("ROW_ID_s").toString() : "");
                    docs.setEXTNAME_s(map.get("EXTNAME_s") != null ? map.get("EXTNAME_s").toString(): "");
                    docs.setASSETNUM_s(map.get("ASSETNUM_s") != null ? map.get("ASSETNUM_s").toString(): "");
                    docs.setPRIMARYID_s(map.get("PRIMARYID_s") != null ? map.get("PRIMARYID_s").toString() : "");
                    docs.setROOTSERVICEID_s(map.get("ROOTSERVICEID_s") != null ? map.get("ROOTSERVICEID_s").toString() : "");
                    docs.setDATECOL1_s(map.get("DATECOL1_s") != null ? map.get("DATECOL1_s").toString() : "");
                    docs.setADDRESS_s(map.get("ADDRESS_s") != null ? map.get("ADDRESS_s").toString() :"");
                    docs.setOCN_s(map.get("OCN_s") != null ? map.get("OCN_s").toString(): "");
                    docs.setREGDATE1_s(map.get("REGDATE1_s") != null ? map.get("REGDATE1_s").toString() :"");
                    docs.setLEGALNAME_s(map.get("LEGALNAME_s") != null ? map.get("LEGALNAME_s").toString(): "");
                    docs.set_version(map.get("_version_") != null ? map.get("_version_").toString():"");
                    realm.commitTransaction();
                }
                fetchFromRealm(realm);
            }
        }
    }

    void fetchFromRealm(Realm realm) {
        if (realm != null) {
            docsArray.clear();
            List<Docs> companyInfo = realm.where(Docs.class).findAll();
            for( Docs obj : companyInfo) {
                HashMap<String, String> map = new HashMap<String,String>();
                map.put("ORDERNUM_s" , obj.getORDERNUM_s());
                map.put("OUNUM_s" , obj.getOUNUM_s());
                map.put("id" , obj.getId());
                map.put("CONNECTIONTYPE_s" , obj.getCONNECTIONTYPE_s());
                map.put("SERIALNUM_s" , obj.getSERIALNUM_s());
                map.put("ASSETROWID_s" , obj.getASSETROWID_s());
                map.put("DATECOL_dt" , obj.getDATECOL_dt());
                map.put("NAMEREF_s" , obj.getNAMEREF_s());
                map.put("REGDATE_dt" , obj.getREGDATE_dt());
                map.put("ROOTASSET_s" , obj.getROOTASSET_s());
                map.put("DESCTEXT_s" , obj.getDESCTEXT_s());
                map.put("BCN_s" , obj.getBCN_s());
                map.put("ROW_ID_s" , obj.getROW_ID_s());
                map.put("EXTNAME_s" , obj.getEXTNAME_s());
                map.put("ASSETNUM_s" , obj.getASSETNUM_s());
                map.put("PRIMARYID_s" , obj.getPRIMARYID_s());
                map.put("ROOTSERVICEID_s" , obj.getROOTSERVICEID_s());
                map.put("DATECOL1_s" , obj.getDATECOL1_s());
                map.put("ADDRESS_s" , obj.getADDRESS_s());
                map.put("OCN_s" , obj.getOCN_s());
                map.put("REGDATE1_s" , obj.getREGDATE1_s());
                map.put("LEGALNAME_s" , obj.getLEGALNAME_s());
                map.put("_version_" , obj.get_version());
                docsArray.add(map);
            }
        }
    }
 }

